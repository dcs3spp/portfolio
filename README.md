# Website

This website is built using [Docusaurus 2](https://docusaurus.io/), a modern
static website generator.

The source is hosted in GitLab and deploys to GitLab pages.
For reference, commands are also documented for deploying to
GitHub.

### Installation

```
$ make install
```

### Configuration

The following environmental variables can be configured for deployment:

| **Variable** | **Description**                                 |
| ------------ | ----------------------------------------------- |
| **USER**     | The GitLab or GitHub user id. Defaults to user. |
| **BASE_URL** | The base url. Default to /.                     |

The environment variables are defined in the Makefile with default values.

They can be overriden in the local shell and also in the CI/CD pipeline of
GitLab or GitHub.

### Local Development

```
$ make run
```

This command starts a local development server and opens up a browser window.
Most changes are reflected live without having to restart the server.

### Docker

This command builds a docker image and serves
a production build in the container.

The sequence of commands below, demonstrate how
to build a docker image and start the website
using docker-compose.

```
$ make docker-build
$ make up


# browse the site at http://localhost:3000
# then when finished...

$ make down
```

### Test Deployment

This command performs a production build and starts
the production built website.

```
$ make serve
```

### Build

```
$ yarn build
```

This command generates static content into the `build` directory and can be
served using any static contents hosting service.

### GitHub Deployment

The following commands provide deployment to github using ssh or GitHub pages.

Using SSH:

```
$ make deploy-ssh-ghub
```

Using GitHub Pages:

```
$ make deploy-ghub
```

If you are using GitHub pages for hosting, this command is a convenient way to
build the website and push to the `gh-pages` branch.
