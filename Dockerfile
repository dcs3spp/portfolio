FROM node:18.13.0-alpine3.17 as builder

WORKDIR /build/website

COPY website/package.json /build/website

RUN yarn install


FROM node:18.13.0-alpine3.17

WORKDIR /app/website

EXPOSE 3000

COPY website/ /app/website/
COPY --from=builder /build/website/ /app/website/

CMD ["yarn", "docusaurus", "serve", "--host", "0.0.0.0"]

