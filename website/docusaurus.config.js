// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion

const { themes } = require("prism-react-renderer");
const lightCodeTheme = themes.github;
const darkCodeTheme = themes.dracula;

/** @type {import('@docusaurus/types').Config} */
const config = {
  title: "SPears",
  tagline: "Development and Documentation Portfolio",
  url: "https://your-docusaurus-test-site.com",
  baseUrl: process.env.BASE_URL || "/",
  onBrokenLinks: "throw",
  onBrokenMarkdownLinks: "warn",
  favicon: "img/favicon.ico",

  // GitHub and GitLab pages deployment config.
  // If you aren't using GitHub or GitLab to deploy, you don't need these.
  organizationName: process.env.USER_ID || "user", // Usually your GitHub/GitLab org/user name.
  projectName: "portfolio", // Usually your repo name.

  customFields: {
    // Put custom environment here for use in react jsx etc.
  },

  // Even if you don't use internalization, you can use this field to set useful
  // metadata like html lang. For example, if your site is Chinese, you may want
  // to replace "en" with "zh-Hans".
  i18n: {
    defaultLocale: "en",
    locales: ["en"],
  },
  presets: [
    [
      "classic",
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        docs: {
          sidebarPath: require.resolve("./sidebars.js"),
        },
        blog: {
          blogSidebarTitle: "My posts",
          blogSidebarCount: "ALL",
          showReadingTime: true,
        },
        theme: {
          customCss: require.resolve("./src/css/custom.css"),
        },
      }),
    ],
  ],

  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
      navbar: {
        title: "Portfolio",
        logo: {
          alt: "Site Logo",
          src: "img/pear.svg",
        },
        items: [
          {
            to: "blog",
            label: "Blog",
            position: "left",
          },
          {
            type: "docSidebar",
            sidebarId: "development",
            position: "left",
            label: "Development",
          },
          {
            type: "docSidebar",
            sidebarId: "documentation",
            position: "left",
            label: "Documentation",
          },
          {
            type: "docSidebar",
            sidebarId: "education",
            position: "left",
            label: "Education",
          },
          {
            type: "dropdown",
            label: "Resume",
            position: "right",
            items: [
              {
                label: "Technical Author",
                to: "https://tinyurl.com/54jyv9jr",
              },
              {
                label: "Software Developer",
                to: "https://tinyurl.com/ymytvnd2",
              },
            ],
          },
          {
            href: "https://roughlea.wordpress.com/raspberry-pi-experiences/configure-the-raspberry-pi-to-share-a-linux-internet-connection/",
            className: "navbar__icon navbar__wordpress",
            "aria-label": "WordPress",
            position: "right",
          },
          {
            href: "https://gitlab.com/groups/sppears_grp/-/shared",
            className: "navbar__icon navbar__gitlab",
            "aria-label": "GitLab Profile",
            position: "right",
          },
          {
            href: `https://github.com/dcs3spp`,
            className: "navbar__icon navbar__github",
            "aria-label": "GitHub Profile",
            position: "right",
          },
        ],
      },
      footer: {
        style: "dark",
        links: [
          {
            title: "Portfolio",
            items: [
              {
                label: "Development",
                to: "/docs/development/experience",
              },
              {
                label: "Documentation",
                to: "/docs/documentation/doc",
              },
              {
                label: "Education",
                to: "/docs/education/academic_performance",
              },
            ],
          },
          {
            title: "Community",
            items: [
              {
                label: "WordPress",
                to: "https://roughlea.wordpress.com/raspberry-pi-experiences/configure-the-raspberry-pi-to-share-a-linux-internet-connection/",
              },
              {
                label: "Stack Overflow",
                to: "https://stackoverflow.com/users/12152982/anon-dcs3spp",
              },
            ],
          },
          {
            title: "Source Code",
            items: [
              {
                label: "GitLab",
                to: "https://gitlab.com/groups/sppears_grp/-/shared",
              },
              {
                label: "GitHub",
                to: "https://github.com/dcs3spp",
              },
            ],
          },
          {
            title: "Songwriting",
            items: [
              {
                label: "Spotify",
                to: "https://open.spotify.com/album/2SI9qomcYhB1rLuHv5d77t",
              },
              {
                label: "YouTube",
                to: "https://www.youtube.com/@roughlea_music/featured",
              },
              {
                label: "Reverbnation",
                to: "https://www.reverbnation.com/roughlea",
              },
            ],
          },
          {
            title: "Contact",
            items: [
              {
                label: "Email",
                to: "mailto: sppears18@gmail.com",
              },
            ],
          },
        ],
        copyright: `Copyright © ${new Date().getFullYear()} SPears.`,
      },
      prism: {
        theme: lightCodeTheme,
        darkTheme: darkCodeTheme,
        additionalLanguages: ["bash", "json", "vim"],
      },
    }),
};

module.exports = config;
