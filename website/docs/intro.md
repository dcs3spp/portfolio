---
sidebar_position: 1
---

# Portfolio

This website provides a reflection on my technical writing skills and
experience, concerning: software documentation, software development and
industrial leadership. A personal reflection of my education is also provided.

The [documentation](documentation/doc) section provides an overview and insight
relating to my experience as a technical author within the software sector. This
includes industrial leadership experiences where I reflect on examples for how I
have engaged in discussions and influenced others.

The [development](development/experience) section describes my technical
software development skills. This includes examples of my knowledge and
experience of programming languages and technologies. Additionally I also
reflect on my open-source skills and give insights into how open-source projects
should be managed.

The [education](education/academic_performance.md) section provides a personal
reflection of my education, including academic performance, hobbies and
experiences.

Example projects include:

- [Python/Angular Web Application](https://www.youtube.com/watch?v=u1yRHivaANQ)
- [Pre-commit Hook To Validate GitLab Ci Yaml Files](https://github.com/dcs3spp/validate-gitlab-ci)
- [Webcam Stream Object Detection](https://gitlab.com/cctv_web_cam/shinobi/objectdetection_mqtt_plugin)
