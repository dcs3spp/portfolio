---
sidebar_label: "Industry Leadership"
sidebar_position: 2
---

# Industry Leadership

So far, I have explained my knowledge and experience with respect to documenting
and developing software products. This section outlines my leadership
experience, including: speaking experience, examples where I have engaged in
public discussion and details of how I have influenced others.

### Describe any speaking experience at industry events and conferences.

During the course of my software development career, I have conducted product
demonstrations of key features which I have developed for each sprint cycle. I
have demonstrated new product features to target audiences, including:
management, developers and users of all levels. I have also conducted
demonstrations remotely via Google Meet and Zoom. Demonstrations include:
overview of APIs, installations and case study applications for knowledge
sharing.

As a teacher, I have prepared learning materials and tutorial exercises for
programming, database design and systems analysis and design. I have delivered
these in lectures and demonstrations to undergraduate students. Furthermore,
within the local education college I was responsible for specifying an inventory
of hardware to enable building Raspberry Pi units for use within the course.
Subsequently, I conducted a demonstration to enable teaching staff and course
leaders to setup and configure the Raspberry Pi.

During my PhD, I submitted technical papers for presentation at international
conferences to a variety of large audiences. This involved presenting an
overview of my research paper, in addition to analysing and evaluating results.

### Are you engaged in public discussion, for example through speaking, writing or even social media) about software and technology? What areas of technology do you focus on?

As a software developer, I have engaged in public forums, asking questions and
raising issues on Stack Overflow and GitHub discussions. My questions typically
range from subject areas such as programming languages and library usage.

I have a personal software repository that implements a Blazor server dashboard
page to display motion detection notifications from a webcam stream. Blazor
provides the [bUnit](https://github.com/bUnit-dev/bUnit#readme) testing library.
I posted a support question on GitHub discussions relating to correct usage of
the library for providing an integration/functional testing context.
Consequently, after discussion I developed a [reusable test
library](https://gitlab.com/sppears_grp/dotnet-blazorserver-testing-with-inprocess-host)
for use with an in-process generic ASP.NET Core host. Please take a moment to
read through the
[discussion](https://github.com/bUnit-dev/bUnit/discussions/268) and the
[readme](https://gitlab.com/sppears_grp/dotnet-blazorserver-testing-with-inprocess-host)
file for the repository that I developed.

I have developed a [Ruby Gem](https://rubygems.org/gems/gitlab-lint-client) to
allow developers to lint a GitLab CI yaml file. I encountered an issue when
developing and testing. For unit testing with RSpec, I wanted to mock standard
input arguments to test the command line interface. However, after discussion on
[Stack
Overflow](https://stackoverflow.com/questions/62830580/how-to-test-optionparser-with-rspec-rspec-options-are-stored-in-argv-array-dur),
I found that the Ruby RSpec test runner appends test parameters to standard
input.

In some cases, I have raised a question, discovered the answer and then posted a
solution of my findings. For example, I asked a question on [Stack
Overflow](https://stackoverflow.com/questions/64175979/mqttnet-unable-to-connect-with-tls-interopapplecryptosslexception-bad-pro)
concerning how to connect to a Mosquitto MQTT broker using TLS from an MQTTnet
client on MacOS. I later discovered, at the time of writing, that TLS v1.3 was
not supported with MQTTnet on MacOS. Subsequently, I posted my findings as a
Stack Overflow answer.

### What influence have you had on others (not just your immediate colleagues) in the industry, through your speaking, writing or other work?

I have some GitLab
[repositories](https://gitlab.com/groups/sppears_grp/-/shared) that use CI
pipelines. Occasionally, I would commit a .gitlab-ci.yml file containing syntax
errors. These would break the CI build.

GitLab provides a web form interface, available per project, for linting.
However, this started to be cumbersome and became a candidate for automation via
the GitLab API and git hooks. Unfortunately, at the time of writing, the free
plan for gitlab.com did not feature pre-receive git server hooks. These could
prevent pushes containing invalid .gitlab-ci.yml files.

Git provides a pre-commit hook that runs for staged files on the local
development environment. While usage of local git hooks is not easily enforced,
I decided that this would be better than the alternative. Subsequently, I
developed a [Ruby gem](https://rubygems.org/gems/gitlab-lint-client) and
[pre-commit](https://github.com/dcs3spp/validate-gitlab-ci) hook to allow
developers to automate linting of a .gitlab-ci.yml file for local commits. At
the time of writing the Ruby gem has 2,534 downloads.

Furthermore, as a developer, I have asked questions in community forums and
GitHub discussion groups concerning software library usage. In some cases, this
has piqued interest with the library developer or project maintainer. This has
resulted in requesting show and tell examples of scenarios for product usage.
Please take a moment to read through this
[discussion](https://github.com/bUnit-dev/bUnit/discussions/268) as an example.
