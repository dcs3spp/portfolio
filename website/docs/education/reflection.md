---
sidebar_label: "Reflections"
sidebar_position: 2
---

# Reflections

### What sort of high school student were you? Beyond your studies, what were your interests and hobbies? How do you think you are remembered by your peers?

I was a conscientious, inquisitive student who demonstrated a willingness to
learn in both collaborative and independent activities.

My main hobby and interest was music. At an extra-curricular level, I was an
active member of school and regional wind bands, playing trumpet. I enjoyed the
collaboration with others to perform a variety of musical compositions. Being a
member of a band required practice, dedication and attention to detail. These
skills allowed me to understand how the part that I played contributed to the
musical composition as a whole. During this time, we participated in
competitions, travelling and performing on evenings and weekends.

I would like to think that I will be remembered by my peers as being
considerate, humorous, studious and trustworthy.

### What were your extracurricular interests and how did you spend your free time?

My main extracurricular interest was music. I participated in the Durham
regional brass wind band, playing principal trumpet. We travelled and performed
at concerts on weekends. I also enjoyed songwriting, cycling and reading.

### What did you have to overcome to attain your successes in education? What are you proudest of?

The successful completion of my PhD took considerable effort to manage and
co-ordinate my studies with teaching duties. I moved to a different university
from where I completed my degree. During this period, I supported myself by
teaching programming and data structures to first year undergraduates. This
involved preparing written content such as tutorials, reference material and
model answers. My peers and I independently reviewed and verified our course
learning materials for quality assurance, before official release to
undergraduates. At the end of the academic year, I reflected, reviewed and
updated learning content, using feedback from learners, my peers and myself.
These skills proved useful in later years during my time as a full-time teacher
in further education.

My proudest achievement during my time in education is the completion of my PhD.
This developed my critical thinking and technical writing skills, preparing
concise structured journals and conference papers for presentation to wide
international audiences.

### If you could have that time again, what would you do differently?

The past has made me who I am today. It has allowed me to develop through self
reflection to overcome challenges. In essence, the past has enabled me to learn
from experience and look forward to the future, planning for what is yet to
come.
