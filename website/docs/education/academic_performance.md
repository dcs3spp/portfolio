---
sidebar_label: "Academic Performance"
sidebar_position: 1
---

# Academic Performance

### Can you describe any high school achievements that would be considered exceptional by peers or teachers or by yourself?

I studied computer science at A-level, achieving grade A. This included a
practical assessment that enabled me to demonstrate application of the software
development life cycle, to create and document a real world database
application. This was my first experience of developing software and required
submitting a written requirements specification document with accompanying
analysis and database design models.

My father worked at the local hospital as a medical electrical engineer,
maintaining and repairing equipment. This provided me with my coursework
project, a database application for managing medical assets and planning an
associated maintenance schedule. I was able to conduct interviews with my father
to analyse the data capture requirements and understand the process of planned
preventive maintenance. I then summarised and documented the workflows and
processes as part of my coursework. Subsequently, I was awarded a grade A\* for
the coursework element.

### If you completed a bachelors degree or equivalent: which degree and university did you choose, and why?

I am educated in computer science, with a BSc (hons) Computing degree from
Sunderland University. I chose to study at Sunderland University since the
course included a one year industrial placement programme. I viewed this as an
opportunity to gain practical industrial experience and identify what would be
the focus of my future career. Subsequently, I successfully completed a PhD from
Durham University. I chose to study at Durham University due to the reputation
and opportunity to study the field of fault tolerant distributed computing.

### How did you perform in your degree, and what was your final degree result? (Note that different education traditions around the world use different scoring systems. Please give us additional context so that we understand what your degree result indicates, even if we're not familiar with that particular system.)

At Sunderland University I successfully completed the BSc (hons) Computing
course, being awarded 1st class. This includes a year of work experience as a
database developer and PC help desk support officer. In my final year I
completed a mobile agent information retrieval research project. This gave me
exposure to independent research. Subsequently, I secured a place at Durham
University where I was awarded a PhD in fault tolerant distributed computing for
mobile agents. The successful completion of the PhD required conducting a
written literature survey to identify a specific problem within a given domain.
Subsequently, I designed and implemented various experiments to evaluate
candidate solutions. These were concisely presented in technical papers that I
presented at international conferences. Consequently, I wrote and submitted my
PhD thesis for review by a panel of subject experts. I presented and defended my
thesis to the panel of experts during my viva and was awarded a PhD.

### Can you describe something you did while in education that made a difference to other people?

The second year of the computing degree required completing a real world group
project for the AA (Automobile Association). Students were organised into teams
and provided with a document that included a written explanation of the
workflow, processes and problems experienced by the AA with their existing
legacy system.

Teams competed to win the contract for development, producing a requirements
specification document and system architecture design. It was intended that the
document from each team was collected and reviewed by internal representatives
of the AA. The winning team would then be selected and announced.

Each team was allocated time to conduct interviews with a representative from
the AA. This was an opportunity to ask clarification questions and analyse
requirements. Within my allocated team, I was responsible for database design.
Subsequently, I produced a database design specification that included proposed
models for data capture, validation, retrieval and analysis. The team
collaboratively produced an invitation to tender document. This summarised the
proposed system architecture and design. Our team won the contract, with our
design being selected for implementation. Subsequently, this allowed the AA to
effectively track and manage their project assets and deliverables.

### Can you describe any achievements at university that would be considered exceptional by peers or teachers - or by yourself?

My primary achievement was successfully completing my PhD. This took
considerable effort and research to independently identify a problem within the
field of fault tolerant distributed computing for mobile agents. I designed and
evaluated candidate solution algorithms that were summarised and presented in
technical papers. These were reviewed and selected by experts for presentation
at various international conferences. I gained valuable feedback from my peers
and experts while presenting and attending international conferences.
Subsequently, I documented the problem, my research findings and solution for
review by a panel of subject experts. I presented and defended a summary to
these experts during my viva and was subsequently awarded the PhD.
