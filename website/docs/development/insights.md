---
sidebar_label: "Insights"
sidebar_position: 1
---

# Insights

This section explains insights gained during the course of my software
development career.

### Would you describe yourself as a high quality coder? Why?

I believe that a high quality coder should strive to have a good understanding
of the problem domain and requirements. Furthermore, problem solving and
critical thinking skills are vital to evaluate a range of architectural
solutions.

To this end, I have collaborated with my peers, users and management at all
levels. This enabled me to analyse requirements and understand the nature of the
application problem. Furthermore, this has also involved reading and analysing
business information from a variety of requirements and technical specification
documents.

During the course of my career, I have produced code that possess the following
traits:

- Self documenting and simple to understand.
- Behaves as expected.
- Clearly reports error messages.
- Adheres to style guidelines.

Subsequently, I have implemented high quality code by adopting the following
principles:

- Use git hooks to apply automated linting and style formatting. I have used the
  following tools to achieve this during the course of my career: flake8, black
  and pre-commit. By adopting this principle I have produced clear and consistent
  code.
- I have created and maintained README documentation. This provides an overview
  of the architecture and explains how to configure, build and install the
  software. When implementing APIs I have used swagger tools to self document
  code, using comments and endpoint specification parameters.
- I have designed and implemented automated unit, integration and end-to-end
  tests that exceed test coverage metrics. My
  [development](https://gitlab.com/groups/sppears_grp/-/shared) portfolio
  highlights the use of a variety of testing frameworks, including: Pytest, XUnit,
  NUnit and Jest. Furthermore, I have used docker and docker-compose to provide
  replicable external dependencies when testing. Examples include databases and
  message queues.
- I have made pull requests for code reviews to gain feedback from my peers.
  Based on feedback, I have updated my source code and documentation before
  merging into development and production branches for release. Similarly, I
  have also performed code reviews on the behalf of my peers.
- I have designed and implemented Continuous Integration (CI) and Continuous
  Delivery (CD) pipelines using GitHub Actions and GitLab CI. These pipelines
  build, test and release versioned software and documentation to development
  and production environments. This allows building and testing software in a
  replicable remote environment before deployment.

In summary, I believe that a good quality coder should perform multiple roles
including analysis, testing, coding and deployment.

### Do you have any thoughts on how open-source software projects should be managed, to be successful?

Open source projects are remotely distributed with contributions from a wide
variety of international developers and users. Subsequently, it is especially
important to establish best practices for managing communication and procedures.
Examples are:

- Establish a project maintainer to oversee the project, triage bugs and
  document processes and guidelines etc.
- Establish a developer road map to give an overview of the technical
  objectives.
- Include a code of conduct document to establish acceptable behaviour,
  procedures and guidelines.
- Include a readme file with a project overview, incorporating an explanatory
  guide for features, installation and configuration.
- Maintain change logs to summarise product evolution, documenting bug fixes
  and breaking changes.
- Provide documentation that explains how to report a security vulnerability.
- Create templates for raising issues and pull requests.
- Establish and document contribution guidelines. These should explain how
  contributions are reviewed, the type of contributions accepted, the expected
  response time and priority of issues for which contributions are accepted.
- Support guidelines that detail how the project is maintained. This includes
  details for how to report bugs and raise feature requests. Furthermore,
  links to community groups and forums should be included.
- Provide a website for documentation that contains a landing page with sections
  for guidelines, categorised into how-to guides, tutorials, reference
  documentation and explanatory materials. This acts as a clear entry point for
  the project.
