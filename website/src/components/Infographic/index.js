import React from "react";
import InfographicSVG from "./infographic.svg";

export default function Infographic() {
  return (
    <div id="infographic">
      <InfographicSVG />
    </div>
  );
}
