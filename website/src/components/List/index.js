import styles from "@site/src/components/List/list.module.css";

import React from "react";

export const List = (props) => {
  const { children } = props;

  return <ul className={styles.list}>{children}</ul>;
};

export const ListItem = (props) => {
  const { children } = props;

  return <li className={styles.listItem}>{children}</li>;
};

export const ListItemButton = (props) => {
  const { children } = props;

  return (
    <a
      className={styles.listItemLink}
      href={props.href}
      target="_blank"
      rel="noopener"
    >
      {children}
      <span className={styles.listItemButton}></span>
    </a>
  );
};

export const ListItemText = (props) => {
  return (
    <div className={styles.listItemText}>
      <span className={styles.listItemTextPrimary}>{props.primary}</span>
      <p className={styles.listItemTextSecondary}>{props.secondary}</p>
    </div>
  );
};
