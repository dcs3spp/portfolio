import styles from "@site/src/components/LandingPage/landing_page.module.css";

import React from "react";

import Projects from "@site/src/components/Projects";
import Infographic from "@site/src/components/Infographic";
import Introduction from "@site/src/components/Introduction";

export default function LandingPage() {
  return (
    <div className={styles.stack}>
      <Introduction />
      <Infographic />
      <Projects projects={projects()} />
    </div>
  );
}

const projects = () => {
  return [
    {
      key: "Blog",
      description: "My Linux blog",
      href: "https://roughlea.wordpress.com/raspberry-pi-experiences/configure-the-raspberry-pi-to-share-a-linux-internet-connection/",
    },
    {
      key: "OpenAPI",
      description: "OpenAPI swagger demo using FastAPI",
      href: "https://gitlab.com/dcs3spp/fastapi_openapi",
    },
    {
      key: "Python",
      description:
        "Python and Angular 2 web application for managing course assessments",
      href: "https://www.youtube.com/watch?v=u1yRHivaANQ",
    },
    {
      key: "Blazor",
      description:
        "Blazor server dashboard to display object detections from webcam MQTT broker",
      href: "https://gitlab.com/dcs3spp/blazormotiondetectionlistener/-/blob/master/README.md",
    },
    {
      key: "IoT",
      description:
        "Enhanced Shinobi Tensorflow module to notify MQTT of webcam object detections",
      href: "https://gitlab.com/cctv_web_cam/shinobi/objectdetection_mqtt_plugin/-/blob/master/README.md",
    },
    {
      key: "Music",
      description: "My Spotify artist profile with examples of my music",
      href: "https://open.spotify.com/artist/2MYUAdwSdF3I3UBpu7HrxP",
    },
  ];
};
