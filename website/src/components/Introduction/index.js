import styles from "@site/src/components/Introduction/introduction.module.css";

import React from "react";

export default function Introduction() {
  return (
    <section>
      <h4 className={styles.title}>S.Pears Profile</h4>
      <div align="justify">
        <p className={styles.bodyText}>
          Hi and welcome to my personal profile. Click on the segments of the
          infographic wheel below to view practical demonstrations of my
          projects. Alternatively, use the descriptions and buttons listed below
          the infographic wheel. A copy of my CV is available within the resume
          menu.
        </p>
        <p className={styles.bodyText}>
          I have over five years experience as a Python and C# backend
          developer, technical author and musician. I have written a range of
          technical and user documents including: reference documentation,
          how-to guides, tutorials, explanatory materials and papers for
          presentation at international conferences. Visit the{" "}
          <a
            className="MuiTypography-root MuiTypography-inherit MuiLink-root MuiLink-underlineAlways css-15kjgm"
            href="/docs/development/experience"
          >
            development
          </a>{" "}
          and{" "}
          <a
            className="MuiTypography-root MuiTypography-inherit MuiLink-root MuiLink-underlineAlways css-15kjgm"
            href="/docs/documentation/doc"
          >
            documentation
          </a>{" "}
          sections to gain further insights and an overview of my experiences.
        </p>
        <p className={styles.bodyText}>
          Further information on my academic history and a reflection of my
          education is available by visiting the{" "}
          <a
            className="MuiTypography-root MuiTypography-inherit MuiLink-root MuiLink-underlineAlways css-15kjgm"
            href="/docs/education/academic_performance"
          >
            education
          </a>{" "}
          section. For those with an interest in music, click on the music
          segment in the wheel below to visit my Spotify artist page.
        </p>
        <p className={styles.bodyText}>
          This website has been developed using{" "}
          <a
            className="MuiTypography-root MuiTypography-inherit MuiLink-root MuiLink-underlineAlways css-15kjgm"
            href="https://docusaurus.io"
            target="__blank"
          >
            Docusaurus
          </a>
          ,{" "}
          <a
            className="MuiTypography-root MuiTypography-inherit MuiLink-root MuiLink-underlineAlways css-15kjgm"
            href="https://mdxjs.com/docs"
            target="__blank"
          >
            MDX
          </a>{" "}
          and{" "}
          <a
            className="MuiTypography-root MuiTypography-inherit MuiLink-root MuiLink-underlineAlways css-15kjgm"
            href="https://reactjs.org/"
            target="__blank"
          >
            React
          </a>
          . The source code is available{" "}
          <a
            className="MuiTypography-root MuiTypography-inherit MuiLink-root MuiLink-underlineAlways css-15kjgm"
            href="https://gitlab.com/dcs3spp/portfolio"
            target="__blank"
          >
            here
          </a>
          .
        </p>
      </div>
    </section>
  );
}
