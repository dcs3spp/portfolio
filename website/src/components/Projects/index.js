import styles from "./projects.module.css";

import {
  List,
  ListItem,
  ListItemButton,
  ListItemText,
} from "@site/src/components/List";

import React from "react";

const Project = ({ project }) => {
  return (
    <ListItem key={project.key}>
      <ListItemButton href={project.href}>
        <ListItemText primary={project.key} secondary={project.description} />
      </ListItemButton>
    </ListItem>
  );
};

export default function Projects({ projects }) {
  let projectList = projects.map((project, index) => (
    <Project key={index} project={project} />
  ));

  return (
    <div className={styles.box}>
      <List>{projectList}</List>
    </div>
  );
}
