export BASE_URL?=/
export USER_ID?=user

docker-build:
	docker-compose build

up:
	docker-compose up

down:
	docker-compose down

build:
	yarn --cwd website build

clean:
	yarn --cwd website docusaurus clear

deploy-ssh-ghub:
	USE_SSH=true yarn --cwd website deploy

deploy-ghub:
	GIT_USER=$USER_ID yarn --cwd website deploy

install:
	yarn --cwd ./website install

run:
	yarn --cwd website docusaurus start

serve:
	yarn --cwd website serve

